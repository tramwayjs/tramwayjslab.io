import jsbeautify from 'js-beautify';
import FormatterProvider from './FormatterProvider';

export default class JavascriptFormatterProvider extends FormatterProvider {
    constructor(config) {
        super();
        this.config = config;
    }

    format(code) {
        return jsbeautify.js_beautify(code, this.config);
    }
}