import routes from './routes';
import routerOptions from './routerOptions';
import * as jsbeautify from './jsbeautify.json';

export {
    routes,
    routerOptions,
    jsbeautify,
}