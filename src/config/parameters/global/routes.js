const routesValues = [
    {
        "methods": ["get"],
        "controller": 'controller.main',
        "title": "Home",
    },
    {
        "controller": 'controller.api',
        "methods": ["get"],
        "path": "/api",
        "title": "API Guide",
    },
    {
        "controller": 'controller.client',
        "methods": ["get"],
        "path": "/client",
        "title": "Client Guide",
    },
    {
        "controller": 'controller.not_found',
        "methods": ["get"],
        "path": "*",
    },
];

export default routesValues;
