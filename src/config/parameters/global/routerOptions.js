const routerOptions = {
    basename: process.env.REACT_APP_PUBLIC_URL
};

export default routerOptions;