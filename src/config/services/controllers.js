import {
    MainController,
    PageNotFoundController,
    ApiController,
    ClientController,
} from '../../controllers';
import {withDependencyInjection} from 'tramway-router-react-strategy';

export default {
    "controller.main": {
        "class": withDependencyInjection(MainController),
        "constructor": [
            {"type": "service", "key": "factory.route_menu_item"}
        ],
    },
    "controller.not_found": {
        "class": withDependencyInjection(PageNotFoundController),
        "constructor": [
            {"type": "service", "key": "factory.route_menu_item"}
        ],
    },
    "controller.api": {
        "class": withDependencyInjection(ApiController),
        "constructor": [
            {"type": "service", "key": "factory.route_menu_item"},
        ],
    },
    "controller.client": {
        "class": withDependencyInjection(ClientController),
        "constructor": [
            {"type": "service", "key": "factory.route_menu_item"}
        ],
    },
}