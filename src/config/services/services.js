import {
    FormatterService,
} from '../../services';

export default {
    "javascript_formatter": {
        "class": FormatterService,
        "constructor": [
            {"type": "service", "key": "provider.javascript_formatter"},
        ],
    },
}
