import {
    RouteMenuItemFactory,
} from '../../factories';

export default {
    "factory.route_menu_item": {
        "class": RouteMenuItemFactory,
        "constructor": [
            {"type": "parameter", "key": "routes"},
        ],
    },
}
