import {
    JavascriptFormatterProvider,
} from '../../providers';

export default {
    "provider.javascript_formatter": {
        "class": JavascriptFormatterProvider,
        "constructor": [
            {"type": "parameter", "key": "jsbeautify"},
        ],
    },
}
