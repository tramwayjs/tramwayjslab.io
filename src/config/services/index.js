import controllers from './controllers';
import router from './router';
import services from './services';
import factories from './factories';
import providers from './providers';

export default {
    ...controllers,
    ...router,
    ...factories,
    ...services,
    ...providers,
}