import React from 'react';
import {Menu} from 'semantic-ui-react';

export default class RouteMenuItemFactory {
    constructor(routes = []) {
        this.routes = Array.isArray(routes) ? routes : Object.values(routes);
    }

    create(activePath) {
        return this.routes.map(({ title, path = '/' }) => title && <Menu.Item as='a' active={path === activePath} href={path}>{title}</Menu.Item>);
    }
}