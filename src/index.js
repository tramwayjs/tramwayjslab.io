import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

import {DependencyResolver, dependencies} from 'tramway-core-react-dependency-injector';
import * as parameters from './config/parameters';
import services from './config/services';

const {ParametersManager, ServicesManager} = dependencies;
DependencyResolver.create(new ServicesManager(), new ParametersManager()).initialize(services, parameters);

let router = DependencyResolver.getService('router').initialize();

ReactDOM.render(router, document.getElementById('root'));
registerServiceWorker();