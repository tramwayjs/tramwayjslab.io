import React from 'react';
import {controllers} from 'tramway-router-react-strategy';
import {ClientPage} from '../components/pages';
const {ReactController} = controllers;

export default class MainController extends ReactController {
    constructor({args: [routeMenuItemFactory], ...props}) {
        super(props);
        this.routeMenuItemFactory = routeMenuItemFactory;
    }

    render() {
        const {pathname} = this.location;
        let routes = this.routeMenuItemFactory.create(pathname);
        return <ClientPage routes={routes}/>
    }
}