import React from 'react';
import {controllers} from 'tramway-router-react-strategy';
import {NotFoundPage} from '../components/pages';
const {NotFoundController} = controllers;

export default class PageNotFoundController extends NotFoundController {
    constructor({args: [routeMenuItemFactory], ...props}) {
        super(props);
        this.routeMenuItemFactory = routeMenuItemFactory;
    }

    render() {
        const {pathname} = this.location;
        let routes = this.routeMenuItemFactory.create(pathname);
        return <NotFoundPage routes={routes}/>
    }
}