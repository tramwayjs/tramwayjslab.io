import React, {PureComponent} from 'react';
import {DesktopSkeleton} from '../../layouts/skeleton';
import {DesktopHeading} from '../../layouts/heading';

export default class DesktopPage extends PureComponent {
    render() {
        const { children, header, footer, routes } = this.props;

        return (
            <DesktopSkeleton>
                <DesktopHeading routes={routes}>
                    {header}
                </DesktopHeading>
                {children}
                {footer}
            </DesktopSkeleton>
        )
    }
}