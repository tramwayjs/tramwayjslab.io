import Page from './Page';
import DesktopPage from './DesktopPage';
import MobilePage from './MobilePage';

export default Page;

export {
    DesktopPage,
    MobilePage,
}