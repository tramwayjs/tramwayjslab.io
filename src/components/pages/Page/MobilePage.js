import React, {PureComponent} from 'react';
import {MobileSkeleton} from '../../layouts/skeleton';
import {MobileHeading} from '../../layouts/heading';

export default class MobilePage extends PureComponent {
    render() {
        const { header, children, footer, routes } = this.props;

        return (
            <MobileSkeleton>
                <MobileHeading 
                    routes={routes}
                    header={header && React.cloneElement(header, {mobile: true, ...header.props})}
                >
                    {children}
                    {footer}
                </MobileHeading>
            </MobileSkeleton>
        )
    }
}