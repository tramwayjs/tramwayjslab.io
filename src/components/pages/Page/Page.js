import React, {PureComponent} from 'react';
import DesktopPage from './DesktopPage';
import MobilePage from './MobilePage';
import {Footer} from '../../sections';

export default class Page extends PureComponent {
    render() {
        const { children, header, routes } = this.props;

        const footer = <Footer/>;

        return (
            <div>
                <DesktopPage header={header} footer={footer} routes={routes}>{children}</DesktopPage>
                <MobilePage header={header} footer={footer} routes={routes}>{children}</MobilePage>
            </div>
        );
    }
}