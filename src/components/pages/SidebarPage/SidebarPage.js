import React from 'react';
import Page from "../Page";
import SidebarDesktopPage from './SidebarDesktopPage';
import SidebarMobilePage from './SidebarMobilePage';
import {Footer} from '../../sections';

export default class SidebarPage extends Page {
    render() {
        const { children, header, routes, navigation } = this.props;

        const footer = <Footer/>;

        return (
            <div>
                <SidebarDesktopPage 
                    header={header} 
                    footer={footer} 
                    routes={routes} 
                    navigation={navigation}
                >
                    {children}
                </SidebarDesktopPage>
                <SidebarMobilePage 
                    header={header} 
                    footer={footer} 
                    routes={routes} 
                    navigation={navigation}
                >
                    {children}
                </SidebarMobilePage>
            </div>
        );
    }
}