import SidebarPage from './SidebarPage';
import SidebarDesktopPage from './SidebarDesktopPage';
import SidebarMobilePage from './SidebarMobilePage';

export default SidebarPage;

export {
    SidebarDesktopPage,
    SidebarMobilePage,
}