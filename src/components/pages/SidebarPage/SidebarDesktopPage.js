import { DesktopPage } from "../Page";
import React from 'react';
import {DesktopSkeleton} from '../../layouts/skeleton';
import {DesktopHeading} from '../../layouts/heading';
import {StickySidebar} from '../../layouts/sticky';
import {Menu} from 'semantic-ui-react';

export default class SidebarDesktopPage extends DesktopPage {
    state = {
        fixed: false,
    };

    setFixed() {
        this.setState({fixed: true});
    }

    unsetFixed() {
        this.setState({fixed: false});
    }

    prepareNavigation(navigation = []) {
        return navigation.map(({title, link}) => <Menu.Item as='a' href={link}>{title}</Menu.Item>);
    }

    render() {
        const { children, header, footer, routes, navigation } = this.props;
        const { fixed } = this.state;

        return (
            <DesktopSkeleton>
                <DesktopHeading 
                    routes={routes}
                    onBottomPassed={() => this.setFixed()}
                    onBottomPassedReverse={() => this.unsetFixed()}
                >
                    {header}
                </DesktopHeading>

                <StickySidebar disabled={!fixed}>
                    {this.prepareNavigation(navigation)}
                </StickySidebar>
                
                {children}
                {footer}
            </DesktopSkeleton>
        )
    }
}