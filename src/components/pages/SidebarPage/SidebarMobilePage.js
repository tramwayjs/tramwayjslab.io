import React from 'react';
import {MobileSkeleton} from '../../layouts/skeleton';
import {MobileHeading} from '../../layouts/heading';
import { MobilePage } from '../Page';
import {Menu} from 'semantic-ui-react';

export default class SidebarMobilePage extends MobilePage {
    prepareNavigation(routes, navigation = []) {
        return routes.filter(a => a).map(route => {
            if (route.props.active) {
                return (
                    <Menu.Item>
                        <Menu.Header>{route.props.children}</Menu.Header>
                        <Menu.Menu>
                            {navigation.map(({title, link}) => <Menu.Item as='a' href={link}>{title}</Menu.Item>)}
                        </Menu.Menu>
                    </Menu.Item>
                );
            }

            return route;
        })
    }

    render() {
        const { header, children, footer, routes, navigation } = this.props;

        return (
            <MobileSkeleton>
                <MobileHeading 
                    routes={this.prepareNavigation(routes, navigation)}
                    header={header && React.cloneElement(header, {mobile: true, ...header.props})}
                >
                    {children}
                    {footer}
                </MobileHeading>
            </MobileSkeleton>
        )
    }
}