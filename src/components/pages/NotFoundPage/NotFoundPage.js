import React, { PureComponent } from 'react';
import Page from '../Page';
import { Heading } from '../../sections';

export default class NotFoundPage extends PureComponent {
    render() {
        const {routes} = this.props;
        return (
            <Page 
                header={(
                    <Heading
                        header='404'
                        subHeader="Sorry but the page you were looking for couldn't be found"
                        callToAction="Go Home"
                        callToActionLink="/"
                    />
                )}
                routes={routes}
            />
        );
    }
}