import LandingPage from './LandingPage';
import Page from './Page';
import ApiPage from './ApiPage';
import ClientPage from './ClientPage';
import NotFoundPage from './NotFoundPage';

export {
    LandingPage,
    Page,
    ApiPage,
    ClientPage,
    NotFoundPage,
}