import React, { PureComponent } from 'react';
import Page from '../Page';
import { Heading } from '../../sections';
import { 
    Statement, 
    Statements,
    HeadingDivider,
    FullLengthSplitStatements,
} from '../../layouts/sections';

export default class LandingPage extends PureComponent {
    render() {
        const {routes} = this.props;
        return (
            <Page 
                header={(
                    <Heading
                        header='tramway.js'
                        subHeader='A collection of libraries to help build your next project'
                        callToAction='Get Started'
                        callToActionLink='/api'
                    />
                )}
                routes={routes}
            >
                <FullLengthSplitStatements>
                    <Statement title="Whether it's an API built with ExpressJS...">
                        You can leverage the common boilerplate and Dependency Injection to quickly get started.
                    </Statement>
                    <Statement title="...Or front-end client built with ReactJS">
                        You can leverage the routing configuration from your API to create corresponding features.
                    </Statement>
                </FullLengthSplitStatements>

                <Statements>
                    <Statement title='SOLID design principles out of the box'>
                        Tramway leverages design patterns to speed up development, giving you the flexibility to add the pieces you'd normally need to boilerplate.
                    </Statement>
                    <Statement title='A collection of opt-in libraries'>
                        Tramway, together with all of its parts, is a modular solution to building applications.
                    </Statement>
                    <HeadingDivider>The Core of Your Application</HeadingDivider>
                    <Statement title='Dependency Injector' callToAction="Get Started" callToActionLink="https://gitlab.com/tramwayjs/tramway-core-dependency-injector" newTab>
                        Increase reusability within your application and other applications with a non-intrusive configuration-driven dependency injection library. Inspired by Symfony's container system, Tramway's Dependency Injection library lets you assign keys to services and parameters and dynamically builds the instances of your services as they become required within your application.
                    </Statement>
                    <Statement title='Routing' callToAction="Get Started" callToActionLink="https://gitlab.com/tramwayjs?utf8=✓&filter=router" newTab>
                        Add a standard configuration layer over Express or React Router to decouple middleware and routing logic, as well as streamline with dependency injection. 
                    </Statement>
                    <Statement title='Providers' callToAction="Get Started" callToActionLink="https://gitlab.com/tramwayjs?utf8=✓&filter=connection" newTab>
                        Streamline your data sources with a consistent interface and leverage dependency injection to ease changes if necessary. Some providers for MySQL, MongoDB, APIs and local caching are ready for you to use.
                    </Statement>
                    <Statement title='Commands' callToAction="Get Started" callToActionLink="https://gitlab.com/tramwayjs?utf8=✓&filter=command" newTab>
                        Inspired by Symfony's commands, Tramway's <code>{'Command'}</code> library allows you to standardize commands with a configurable CLI interface, arguments and options.
                    </Statement>
                    <Statement title='Logging' callToAction="Get Started" callToActionLink="https://gitlab.com/tramwayjs?utf8=✓&filter=log" newTab>
                        Keep track of your application's health and much more with a logger of your choice.
                    </Statement>
                </Statements>
            </Page>
        );
    }
}