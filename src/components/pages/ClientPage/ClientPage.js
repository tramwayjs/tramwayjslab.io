import React, { PureComponent } from 'react';
import SidebarPage from '../SidebarPage';
import { Heading } from '../../sections';

export default class ClientPage extends PureComponent {
    render() {
        const { routes } = this.props;

        const navigation = [
            {title: 'Getting Started', link: ''},
            {title: 'Getting Started', link: ''},
        ]

        return (
            <SidebarPage
                routes={routes}
                header={(
                    <Heading
                        header='Coming soon!'
                        subHeader="This page is currently under construction"
                        callToAction="Go Home"
                        callToActionLink="/"
                    />
                )}
                navigation={navigation}
            >
            </SidebarPage>
        );
    }
}