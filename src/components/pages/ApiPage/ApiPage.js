import React, { PureComponent } from 'react';
import SidebarPage from '../SidebarPage';
import { FolderStructure, CommandBlock } from '../../sections';

import {
    Statement,
    Statements,
    HeadingDivider,
} from '../../layouts/sections';
import { List } from 'semantic-ui-react';
import { SubFolderStructure, File } from '../../sections/FolderStructure/FolderStructure';

export default class ApiPage extends PureComponent {
    render() {
        const {routes} = this.props;

        const navigation = [
            {title: 'Getting started', link: ''},
            {title: 'Folder structure', link: ''},
            {title: 'Create API', link: ''},
        ]

        return (
            <SidebarPage 
                routes={routes}
                navigation={navigation}
            >
                <Statements>
                    <Statement title='Getting Started'>
                        <List ordered>
                            <List.Item>
                                Install the tramway dev tools
                                <CommandBlock>
                                    npm install --save-dev tramway
                                </CommandBlock>
                            </List.Item>
                            <List.Item>
                                Setup Tramway with one command
                                <CommandBlock>
                                    ./node_modules/.bin/tramway install
                                </CommandBlock>
                                Installs core Tramway libraries, sets up dependency injection configuration and adds the base .babelrc configuration.
                            </List.Item>
                            <List.Item>
                                Build your source code
                                <CommandBlock>
                                    ./node_modules/.bin/tramway build
                                </CommandBlock>
                                Use the babel configuration to create a build that supports your server environment.
                            </List.Item>
                            <List.Item>
                                Start the web server
                                <CommandBlock>
                                    ./node_modules/.bin/tramway start
                                </CommandBlock>
                                Start an environment-aware webserver that will watch for changes to files in development mode.
                            </List.Item>
                        </List>
                    </Statement>
                    <Statement title='Folder structure'>
                        <FolderStructure name='src'>
                            <SubFolderStructure name='config' description=''>
                                <SubFolderStructure name='parameters' description='All parameters inherited by environment compared to global'>
                                    <SubFolderStructure name='global' description='The core parameters that can be inheritted and overrided in different environemnts'>
                                        <File name='index.js' description='The export structure determines the parameter keys used in dependency injection'/>
                                    </SubFolderStructure>
                                </SubFolderStructure>
                                <SubFolderStructure name='services' description='All the configuration necessary for dependency injection'>
                                    <File name='controllers.js'/>
                                    <File name='providers.js'/>
                                    <File name='services.js'/>
                                </SubFolderStructure>
                            </SubFolderStructure>
                            <SubFolderStructure name='controllers'/>
                            <SubFolderStructure name='services'/>
                            <SubFolderStructure name='providers'/>
                        </FolderStructure>
                        <FolderStructure name='dist' description='The output folder for the transpiled application'>
                        </FolderStructure>
                    </Statement>
                    <HeadingDivider>Use Tramway development commands to get started quickly</HeadingDivider>
                    Still experimental, but the following utility commands will write common boilerplate for you.
                    <Statement title='Create API' callToAction="Get Started" callToActionLink="https://gitlab.com/tramwayjs/tramway" newTab>
                        Create the necessary boilerplate for a REST style API
                        <CommandBlock>
                            ./node_modules/.bin/tramway create:api Resource
                        </CommandBlock>
                        This command will create an Entity, REST style routes, Repository, Service and Controller as well as make the necessary registrations in the dependency injection config.
                    </Statement>
                </Statements>
            </SidebarPage>
        );
    }
}