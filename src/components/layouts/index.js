import * as skeleton from './skeleton';
import * as heading from './heading';
import * as sections from './sections';
import * as sticky from './sticky';

export {
    heading,
    skeleton,
    sections,
    sticky,
}