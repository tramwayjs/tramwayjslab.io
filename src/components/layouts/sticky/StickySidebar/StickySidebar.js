import React, {PureComponent} from 'react';
import {Menu} from 'semantic-ui-react';

export default class StickySidebar extends PureComponent {
    render() {
        const {children, top, disabled} = this.props;

        let position, zIndex;

        if (disabled) {
            position = 'relative';
            zIndex = 'auto';
        }

        return (
            <div className='sticky-sidebar' style={{top, position, zIndex}}>
                <Menu
                    className='sticky-sidebar-content'
                    vertical
                >
                    {children}
                </Menu>
            </div>
        );
    }
}