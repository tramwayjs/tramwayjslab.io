import React, {PureComponent} from 'react';
import {Sidebar, Menu, Container, Icon, Segment} from 'semantic-ui-react';
import {NavigationExtra} from '../../sections';

export default class MobileHeading extends PureComponent {
    state = {}

    handleToggle() {
        return this.setState({ sidebarOpened: !this.state.sidebarOpened });
    }

    handlePusherClick() {
        const { sidebarOpened } = this.state;

        if (sidebarOpened) {
            this.setState({ sidebarOpened: false });
        }
    }

    render() {
        const {routes, children, header} = this.props;
        const {sidebarOpened} = this.state;

        return (
            <Sidebar.Pushable>
                <Sidebar as={Menu} animation='uncover' inverted vertical visible={sidebarOpened}>
                    {routes}
                </Sidebar>

                <Sidebar.Pusher
                    className='heading'
                    dimmed={sidebarOpened}
                    onClick={() => this.handlePusherClick()}
                >
                    <Segment
                        className={header && 'heading mobile'}
                        inverted
                        textAlign='center'
                        vertical
                    >
                        <Container>
                            <Menu inverted pointing secondary size='large'>
                                <Menu.Item onClick={() => this.handleToggle()}>
                                    <Icon name='sidebar'/>
                                </Menu.Item>
                                <NavigationExtra/>
                            </Menu>
                        </Container>
                        {header}
                    </Segment>
                    {children}
                </Sidebar.Pusher>
            </Sidebar.Pushable>
        );
    }
}