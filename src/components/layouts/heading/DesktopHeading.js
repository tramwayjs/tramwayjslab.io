import React, {PureComponent} from 'react';
import {Visibility, Segment} from 'semantic-ui-react';
import {Navigation, NavigationExtra} from '../../sections';

export default class DesktopHeading extends PureComponent {
    state = {
        fixed: false,
    }

    showFixedMenu() {
        return this.setState({ fixed: true }, () => {
            this.props.onBottomPassed && this.props.onBottomPassed();
        });
    }

    hideFixedMenu() {
        return this.setState({ fixed: false }, () => {
            this.props.onBottomPassedReverse && this.props.onBottomPassedReverse();
        });
    }

    render() {
        const {children, routes} = this.props;
        const {fixed} = this.state;

        return (
            <Visibility
                once={false}
                onBottomPassed={() => this.showFixedMenu()}
                onBottomPassedReverse={() => this.hideFixedMenu()}
            >
                <Segment
                    className={children && 'heading'}
                    inverted
                    textAlign='center'
                    vertical
                >
                    <Navigation 
                        fixed={fixed} 
                        routes={routes}
                    >
                        <NavigationExtra fixed={fixed}/>
                    </Navigation>
                    {children}
                </Segment>
            </Visibility>
        );
    }
}