import DesktopHeading from './DesktopHeading';
import MobileHeading from './MobileHeading';
import './Heading.css';

export {
    DesktopHeading,
    MobileHeading,
};