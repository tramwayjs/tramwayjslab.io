import React, {PureComponent} from 'react';
import {Responsive} from 'semantic-ui-react';

export default class DesktopSkeleton extends PureComponent {
    render() {
        const {children} = this.props;
        return (
            <Responsive minWidth={Responsive.onlyTablet.minWidth}>
                {children}
            </Responsive>
        );
    }
}