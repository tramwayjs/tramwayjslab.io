import DesktopSkeleton from './DesktopSkeleton';
import MobileSkeleton from './MobileSkeleton';

export {
    DesktopSkeleton,
    MobileSkeleton,
};