import React, {PureComponent} from 'react';
import {Responsive} from 'semantic-ui-react';

export default class MobileSkeleton extends PureComponent {
    render() {
        const {children} = this.props;
        return (
            <Responsive maxWidth={Responsive.onlyMobile.maxWidth}>
                {children}
            </Responsive>
        );
    }
}