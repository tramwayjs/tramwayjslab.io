import React, {PureComponent} from 'react';
import {Divider} from 'semantic-ui-react';

export default class HeadingDivider extends PureComponent {
    render() {
        const {category, categoryLink, children} = this.props;

        return (
            <Divider
                as='h4'
                className='heading'
                horizontal
            >
                <a href={categoryLink}>{children || category}</a>
            </Divider>
        );
    }
}