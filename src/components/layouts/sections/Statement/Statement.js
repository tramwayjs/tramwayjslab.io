import React, {PureComponent} from 'react';
import {Header, Button} from 'semantic-ui-react';

export default class Statement extends PureComponent {
    render() {
        const {title, content, children, callToAction, callToActionLink, newTab} = this.props;

        let link = {
            href: callToActionLink,
        }

        if (newTab) {
            link.target = "_blank";
        }

        return (
            <React.Fragment>
                <Header as='h3' className='statement'>
                    {title}
                </Header>
                <p className='statement'>
                    {children || content}
                </p>
                {callToAction && (
                    <Button as='a' size='large' {...link}>
                        {callToAction}
                    </Button>
                )}
            </React.Fragment>
        );
    }
}