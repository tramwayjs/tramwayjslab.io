import React, { PureComponent } from 'react';
import {Container, Header, Button, Icon} from 'semantic-ui-react';

export default class Heading extends PureComponent {
    render() {
        const {
            mobile, 
            callToAction,
            callToActionIcon = 'right arrow',
            callToActionLink,
            header,
            subHeader,
        } = this.props;

        return (
            <Container text>
                <Header
                    className={`heading ${mobile ? 'mobile' : ''}`}
                    as='h1'
                    content={header}
                    inverted
                />
                <Header
                    className={`heading ${mobile ? 'mobile' : ''}`}
                    as='h2'
                    content={subHeader}
                    inverted
                />
                <Button as='a' primary size='huge' href={callToActionLink}>
                    {callToAction}
                    <Icon name={callToActionIcon} />
                </Button>
            </Container>
        );
    }
}