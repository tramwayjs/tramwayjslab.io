import React, { PureComponent } from 'react';
import { Menu, Container } from 'semantic-ui-react';

export default class Navigation extends PureComponent {
    render() {
        const { fixed, children, routes } = this.props;

        return (
            <Menu
                fixed={fixed ? 'top' : null}
                inverted={!fixed}
                pointing={!fixed}
                secondary={!fixed}
                size='large'
            >
                <Container>
                    {routes}
                    {children}
                </Container>
            </Menu>
        );
    }
}