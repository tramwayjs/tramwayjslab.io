import React, {PureComponent} from 'react';
import AceEditor from 'react-ace';
import { DependencyResolver } from 'tramway-core-react-dependency-injector';

import 'brace/mode/javascript';
import 'brace/theme/kuroir';

export default class CodeBlock extends PureComponent {
    static defaultProps = {
        mode: "javascript",
        theme: "kuroir",
        children: '',
    }

    constructor(props) {
        super(props);

        this.formatter = DependencyResolver.getService('javascript_formatter');
    }

    render() {
        const {children, mode, theme} = this.props;
        let numLines = children.split(/\r\n|\r|\n/).length;

        return (
            <AceEditor
                className='codeblock'
                width='100%'
                mode={mode}
                theme={theme}
                showPrintMargin
                showGutter
                highlightActiveLine
                value={this.formatter.format(children)}
                maxLines={numLines}
                setOptions={{
                    enableBasicAutocompletion: true,
                    enableLiveAutocompletion: true,
                    enableSnippets: true,
                    showLineNumbers: true,
                    tabSize: 4,
                    useWorker: false,
                    readOnly: true,
                }}
                editorProps={{
                    $blockScrolling: true
                }}
            />
            
        );
    }
}