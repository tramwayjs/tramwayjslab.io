import React, { PureComponent } from 'react';
import { Menu, Button, Icon } from 'semantic-ui-react';

export default class NavigationExtra extends PureComponent {
    render() {
        const {fixed} = this.props;

        return (
            <Menu.Item position='right' className='navigation-extra'>
                <Button as='a' inverted={!fixed} href="https://gitlab.com/tramwayjs">
                    <Icon name="gitlab"/> Source
                </Button>
            </Menu.Item>
        );
    }
}