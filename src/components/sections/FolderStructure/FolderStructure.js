import React, { PureComponent } from 'react';
import { List } from 'semantic-ui-react';

export default class FolderStructure extends PureComponent {
    render() {
        const {name, description, children} = this.props;

        return (
            <List>
                <SubFolderStructure name={name} description={description}>
                    {children}
                </SubFolderStructure>
            </List>
        );
    }
}

export class SubFolderStructure extends PureComponent {
    render() {
        const {name, description, children} = this.props;

        return (
            <List.Item>
                <List.Icon name='folder' />
                <List.Content>
                    <List.Header>{name}</List.Header>
                    <List.Description>{description}</List.Description>
                    {children && <List.List>{children}</List.List>}
                </List.Content>
            </List.Item>
        );
    }
}

export class File extends PureComponent {
    render() {
        const {name, description} = this.props;

        return (
            <List.Item>
                <List.Icon name='file' />
                <List.Content>
                    <List.Header>{name}</List.Header>
                    <List.Description>{description}</List.Description>
                </List.Content>
            </List.Item>
        );
    }
}