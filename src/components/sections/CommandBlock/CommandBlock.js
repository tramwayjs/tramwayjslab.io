import React, {PureComponent} from 'react';
import {Segment} from 'semantic-ui-react';

export default class CommandBlock extends PureComponent {
    render() {
        const {children} = this.props;

        return (
            <Segment vertical basic className='commandblock'>
                <Segment basic className='commandblock inner'>
                    <pre>
                        <code>
                            {children}
                        </code>
                    </pre>
                </Segment>
            </Segment>
        );
    }
}