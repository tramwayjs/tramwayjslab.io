import React, {PureComponent} from 'react';
import {Segment, Container, Grid, List, Header} from 'semantic-ui-react';

export default class Footer extends PureComponent {
    render() {
        return (
            <Segment inverted vertical style={{ padding: '5em 0em' }}>
                <Container>
                    <Grid divided inverted stackable>
                        <Grid.Row>
                            <Grid.Column width={3}>
                                <Header inverted as='h4' content='About' />
                                <List link inverted>
                                    <List.Item as='a' href="https://gitlab.com/tramwayjs" target="_blank">TramwayJS</List.Item>
                                    <List.Item as='a' href="https://gitlab.com/juliancwolfe" target="_blank">Julian Wolfe</List.Item>
                                    <List.Item as='a'>Contact</List.Item>
                                </List>
                            </Grid.Column>
                            <Grid.Column width={3}>
                                <Header inverted as='h4' content='Resources' />
                                <List link inverted>
                                    <List.Item as='a' href="https://expressjs.com/" target="_blank">ExpressJS</List.Item>
                                    <List.Item as='a' href="https://reactjs.org/" target="_blank">ReactJS</List.Item>
                                    <List.Item as='a' href="https://reacttraining.com/react-router/" target="_blank">React Router</List.Item>
                                    <List.Item as='a' href="https://react.semantic-ui.com/" target="_blank">Semantic UI</List.Item>
                                </List>
                            </Grid.Column>
                            <Grid.Column width={7}>
                                <Header as='h4' inverted>
                                    Built with Tramway
                                </Header>
                                <p>
                                    This site was built with Tramway Router and its React strategy alongside Tramway Dependency Injector and Semantic UI React.
                                </p>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Container>
            </Segment>
        );
    }
}