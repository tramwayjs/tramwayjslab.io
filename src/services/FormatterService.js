export default class FormatterService {
    /**
     * 
     * @param {FormatterProvider} formatter 
     */
    constructor(formatter) {
        this.formatter = formatter;
    }

    /**
     * 
     * @param {string} code 
     * @returns {string}
     */
    format(code) {
        return this.formatter.format(code);
    }
}